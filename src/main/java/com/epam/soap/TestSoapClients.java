package com.epam.soap;

import com.epam.soap.clients.*;
import com.epam.soap.exceptions.AuthorOperationException;
import com.epam.soap.exceptions.BookOperationException;
import com.epam.soap.exceptions.UserOperationException;
import com.epam.soap.interfaces.*;
import com.epam.soap.model.Author;
import com.epam.soap.util.AuthorizationUtil;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class TestSoapClients {
    public static void main(String[] args) {
     //search();
       // admin();
       // bookmark();

        try {
            author();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
    }


    private static void search() {
       // try {
            SearchBookWebService searchBookWebService = new SearchBookWebService();
            SearchBookService search = searchBookWebService.getSearchBookServiceImplPort();
           // search.searchBooksByPartOfName("a").getItem().forEach(System.out::println);
          // search.searchBooksByUserBookmark(8).getItem().forEach(System.out::println);
            search.searchBooksByPartOfAuthorName("a").getItem().forEach(System.out::println);

          //  search.searchBooksByIsbn("000-0000000008").getItem().forEach(System.out::println);

           // search.searchBooksByReleaseYearRange(1, 2020).getItem().forEach(System.out::println);
           // search.searchBooksBySeveralWays(2002, 1275, "Harry Potter");
//        } catch (BookOperationException e) {
//            System.out.println(e.getFaultInfo().getStatus() + "  " + e.getFaultInfo().getMessage());
//        }
    }


    private static void admin() {

        AdminWebService adminWebService = new AdminWebService();
        AdminService adminClient = adminWebService.getAdminServiceImplPort();

        AuthorizationUtil.putDataInRequestHeader(adminWebService,adminClient,"1","1");
        try {

            //adminClient.registerNewUser("roma","123");

           adminClient.getUserHistory(4).getItem().forEach(System.out::println);
          //  adminClient.blockUser(4);

         } catch (UserOperationException e) {
            System.out.println(e.getFaultInfo().getStatus() + "  " + e.getFaultInfo().getMessage());
         }

    }

    private static void author() throws ParseException, DatatypeConfigurationException {

        AuthorWebService authorWebService = new AuthorWebService();
        AuthorService authorClient = authorWebService.getAuthorServiceImplPort();

        GregorianCalendar c = new GregorianCalendar();
        c.setTime(new SimpleDateFormat("yyyy-mm-dd").parse("1999-07-07"));
        AuthorizationUtil.putDataInRequestHeader(authorWebService,authorClient,"1","1");

        try {
            System.out.println(authorClient.addNewAuthor("URa", "lixachov", "", DatatypeFactory.newInstance().newXMLGregorianCalendar(c)));

            System.out.println(authorClient.deleteAuthorWithHisBooks(1));

            authorClient.getAllAuthors().getItem().forEach(System.out::println);


        } catch (AuthorOperationException e) {
            System.out.println(e.getFaultInfo().getStatus() + "  " + e.getFaultInfo().getMessage());
        } catch (UserOperationException e) {
            System.out.println(e.getFaultInfo().getStatus() + "  " + e.getFaultInfo().getMessage());
        }




    }

    private static void bookmark() {
        BookmarkWebService bookmarkWebService = new BookmarkWebService();
        BookmarkService bookmarkClient = bookmarkWebService.getBookmarkServiceImplPort();
        AuthorizationUtil.putDataInRequestHeader(bookmarkWebService,bookmarkClient,"1","1");
        try {
            System.out.println(bookmarkClient.addNewBookmark(4, 8, 440));

          //  bookmarkClient.deleteBookmark(3);
        } catch (BookOperationException e) {
            System.out.println(e.getFaultInfo().getStatus() + "  " + e.getFaultInfo().getMessage());
        } catch (UserOperationException e) {
           System.out.println(e.getFaultInfo().getStatus() + "  " + e.getFaultInfo().getMessage());
        }

    }

    private static void book() {
        BookWebService bookWebService = new BookWebService();
        BookService bookClient = bookWebService.getBookServiceImplPort();
        try {
            bookClient.addNewBook("name",2010,"000-1111111111","Ya",20,new Author());

           // bookClient.deleteBook(2);
        } catch (BookOperationException e) {
            System.out.println(e.getFaultInfo().getStatus() + "  " + e.getFaultInfo().getMessage());
        } catch (UserOperationException e) {
            System.out.println(e.getFaultInfo().getStatus() + "  " + e.getFaultInfo().getMessage());
        }

    }


}
