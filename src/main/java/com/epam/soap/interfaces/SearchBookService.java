
package com.epam.soap.interfaces;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;

import com.epam.soap.exceptions.BookOperationException;
import com.epam.soap.model.BookArray;
import com.epam.soap.model.ObjectFactory;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "SearchBookService", targetNamespace = "http://sei.soap.epam.com/")
@SOAPBinding(style = SOAPBinding.Style.RPC)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface SearchBookService {


    /**
     * 
     * @param arg0
     * @return
     *     returns com.epam.soap.model.BookArray
     */
    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://sei.soap.epam.com/SearchBookService/searchBooksByPartOfNameRequest", output = "http://sei.soap.epam.com/SearchBookService/searchBooksByPartOfNameResponse")
    public BookArray searchBooksByPartOfName(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0);

    /**
     * 
     * @param arg0
     * @return
     *     returns com.epam.soap.model.BookArray
     */
    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://sei.soap.epam.com/SearchBookService/searchBooksByPartOfAuthorNameRequest", output = "http://sei.soap.epam.com/SearchBookService/searchBooksByPartOfAuthorNameResponse")
    public BookArray searchBooksByPartOfAuthorName(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0);

    /**
     * 
     * @param arg1
     * @param arg0
     * @return
     *     returns com.epam.soap.model.BookArray
     * @throws BookOperationException
     */
    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://sei.soap.epam.com/SearchBookService/searchBooksByReleaseYearRangeRequest", output = "http://sei.soap.epam.com/SearchBookService/searchBooksByReleaseYearRangeResponse", fault = {
        @FaultAction(className = BookOperationException.class, value = "http://sei.soap.epam.com/SearchBookService/searchBooksByReleaseYearRange/Fault/BookOperationException")
    })
    public BookArray searchBooksByReleaseYearRange(
        @WebParam(name = "arg0", partName = "arg0")
        int arg0,
        @WebParam(name = "arg1", partName = "arg1")
        int arg1)
        throws BookOperationException
    ;

    /**
     * 
     * @param arg0
     * @return
     *     returns com.epam.soap.model.BookArray
     */
    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://sei.soap.epam.com/SearchBookService/searchBooksByUserBookmarkRequest", output = "http://sei.soap.epam.com/SearchBookService/searchBooksByUserBookmarkResponse")
    public BookArray searchBooksByUserBookmark(
        @WebParam(name = "arg0", partName = "arg0")
        int arg0);

    /**
     * 
     * @param arg2
     * @param arg1
     * @param arg0
     * @return
     *     returns com.epam.soap.model.BookArray
     */
    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://sei.soap.epam.com/SearchBookService/searchBooksBySeveralWaysRequest", output = "http://sei.soap.epam.com/SearchBookService/searchBooksBySeveralWaysResponse")
    public BookArray searchBooksBySeveralWays(
        @WebParam(name = "arg0", partName = "arg0")
        int arg0,
        @WebParam(name = "arg1", partName = "arg1")
        int arg1,
        @WebParam(name = "arg2", partName = "arg2")
        String arg2);

    /**
     * 
     * @param arg0
     * @return
     *     returns com.epam.soap.model.BookArray
     * @throws BookOperationException
     */
    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://sei.soap.epam.com/SearchBookService/searchBooksByIsbnRequest", output = "http://sei.soap.epam.com/SearchBookService/searchBooksByIsbnResponse", fault = {
        @FaultAction(className = BookOperationException.class, value = "http://sei.soap.epam.com/SearchBookService/searchBooksByIsbn/Fault/BookOperationException")
    })
    public BookArray searchBooksByIsbn(
        @WebParam(name = "arg0", partName = "arg0")
        String arg0)
        throws BookOperationException
    ;

}
