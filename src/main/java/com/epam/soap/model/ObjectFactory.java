
package com.epam.soap.model;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.epam.soap.sei package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _HistoryDto_QNAME = new QName("http://sei.soap.epam.com/", "historyDto");
    private final static QName _Message_QNAME = new QName("http://sei.soap.epam.com/", "message");
    private final static QName _UserOperationException_QNAME = new QName("http://sei.soap.epam.com/", "UserOperationException");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.epam.soap.sei
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link History }
     * 
     */
    public History createHistory() {
        return new History();
    }

    /**
     * Create an instance of {@link Message }
     * 
     */
    public Message createMessage() {
        return new Message();
    }

    /**
     * Create an instance of {@link UserOperationException }
     * 
     */
    public UserOperationException createUserOperationException() {
        return new UserOperationException();
    }

    /**
     * Create an instance of {@link HistoryArray }
     * 
     */
    public HistoryArray createHistoryArray() {
        return new HistoryArray();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link History }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.soap.epam.com/", name = "historyDto")
    public JAXBElement<History> createHistoryDto(History value) {
        return new JAXBElement<History>(_HistoryDto_QNAME, History.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Message }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.soap.epam.com/", name = "message")
    public JAXBElement<Message> createMessage(Message value) {
        return new JAXBElement<Message>(_Message_QNAME, Message.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserOperationException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sei.soap.epam.com/", name = "UserOperationException")
    public JAXBElement<UserOperationException> createUserOperationException(UserOperationException value) {
        return new JAXBElement<UserOperationException>(_UserOperationException_QNAME, UserOperationException.class, null, value);
    }

}
