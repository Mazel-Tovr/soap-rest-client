
package com.epam.soap.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for bookmark complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="bookmark">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="bookmarkId" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="userId" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="bookId" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="pageCount" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "bookmark")
public class Bookmark {

    @XmlAttribute(name = "bookmarkId", required = true)
    protected int bookmarkId;
    @XmlAttribute(name = "userId", required = true)
    protected int userId;
    @XmlAttribute(name = "bookId", required = true)
    protected int bookId;
    @XmlAttribute(name = "pageCount", required = true)
    protected int pageCount;

    /**
     * Gets the value of the bookmarkId property.
     * 
     */
    public int getBookmarkId() {
        return bookmarkId;
    }

    /**
     * Sets the value of the bookmarkId property.
     * 
     */
    public void setBookmarkId(int value) {
        this.bookmarkId = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     */
    public int getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     */
    public void setUserId(int value) {
        this.userId = value;
    }

    /**
     * Gets the value of the bookId property.
     * 
     */
    public int getBookId() {
        return bookId;
    }

    /**
     * Sets the value of the bookId property.
     * 
     */
    public void setBookId(int value) {
        this.bookId = value;
    }

    /**
     * Gets the value of the pageCount property.
     * 
     */
    public int getPageCount() {
        return pageCount;
    }

    /**
     * Sets the value of the pageCount property.
     * 
     */
    public void setPageCount(int value) {
        this.pageCount = value;
    }

    @Override
    public String toString() {
        return "Bookmark{" +
                "bookmarkId=" + bookmarkId +
                ", userId=" + userId +
                ", bookId=" + bookId +
                ", pageCount=" + pageCount +
                '}';
    }
}
