package com.epam.rest.clients;

import com.epam.rest.model.History;
import com.epam.rest.model.Token;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class  AdminClient
{
    private static final String URL_PATH ="http://25.89.170.214:8888/home/rest/admin";

    public String registerNewUser( String login , String password, Token token) {

        HttpPost request = new HttpPost(URL_PATH+"/registerNewUser?" +
                "login="+login+"&password="+password+"&token="+token.getToken());

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(request)){

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                return EntityUtils.toString(entity);
            }
        }catch (IOException e)
        {
            e.getStackTrace();
        }
        return "server exception";
    }

    public String blockUser( int userId, Token token)
    {
        HttpPost request = new HttpPost(URL_PATH+"/blockUser?" +
                "user_id="+userId+"&token="+token.getToken());

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(request)){

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                return EntityUtils.toString(entity);
            }
        }catch (IOException e)
        {
            e.getStackTrace();
        }
        return "server exception";
    }


    public List<History> getAll( int userId,  Token token)
    {
        HttpGet request = new HttpGet(URL_PATH+"/getUserHistory?user_id="+userId+"&token="+token.getToken());

        String result ="server error";
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(request)){

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                result = EntityUtils.toString(entity);
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();

                History[] authors = gson.fromJson(result, History[].class);

                return Arrays.asList(authors);
            }
        }catch ( Exception e)
        {
            System.out.println(result);;
        }
        return null;
    }
}
