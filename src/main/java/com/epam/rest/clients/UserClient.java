package com.epam.rest.clients;

import com.epam.rest.model.Token;
import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;


public class UserClient {

    private static final String URL_PATH ="http://25.89.170.214:8888/home/rest/users";

    public Token login(String login, String password) throws UnsupportedEncodingException {

        HttpPost request = new HttpPost(URL_PATH+"/login?login="+login+"&password="+password);

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(request)){

            // Get HttpResponse Status
//            System.out.println(response.getProtocolVersion());              // HTTP/1.1
//            System.out.println(response.getStatusLine().getStatusCode());   // 200
//            System.out.println(response.getStatusLine().getReasonPhrase()); // OK
//            System.out.println(response.getStatusLine().toString());        // HTTP/1.1 200 OK

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                String result = EntityUtils.toString(entity);
                Gson gson = new Gson();
              return gson.fromJson(result, Token.class);

            }
        }catch (IOException e)
        {
            e.getStackTrace();
        }
        return null;
    }

}
