package com.epam.rest.clients;

import com.epam.rest.model.Author;
import com.epam.rest.model.Token;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class AuthorClient
{
    private static final String URL_PATH ="http://25.89.170.214:8888/home/rest/authors";

    public String addAuthor( String firstName, String secondName, String lastName, String dob,  Token token)
    {
        HttpPost request = new HttpPost(URL_PATH+"/addAuthor?" +
                "first_name="+firstName+"&second_name="+secondName+"&last_name="+lastName+"&dob="+dob+"&token="+token.getToken());

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(request)){



            HttpEntity entity = response.getEntity();
            if (entity != null) {
                return EntityUtils.toString(entity);
            }
        }catch (IOException e)
        {
            e.getStackTrace();
        }
        return "server exception";
    }

    public String deleteAuthor( int authorId,  Token token)
    {
        HttpPost request = new HttpPost(URL_PATH+"/deleteAuthor?" +
                "author_id="+authorId+"&token="+token.getToken());

        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(request)){

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                return EntityUtils.toString(entity);
            }
        }catch (IOException e)
        {
            e.getStackTrace();
        }
        return "server exception";
    }
    public List<Author> getAll()
    {
        HttpGet request = new HttpGet(URL_PATH+"/getAllAuthors");

        String result ="server error";
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(request)){

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                result = EntityUtils.toString(entity);
                GsonBuilder builder = new GsonBuilder().setDateFormat("mm.dd.yyyy");
                Gson gson = builder.create();

                Author[] authors = gson.fromJson(result, Author[].class);

                return Arrays.asList(authors);
            }
        }catch ( Exception e)
        {
            System.out.println(result);;
        }
        return null;
    }

}
