package com.epam.rest.clients;

import com.epam.rest.model.Book;
import com.epam.rest.model.ErrorResponseInfo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SearchClient {

    private static final String URL_PATH = "http://25.89.170.214:8888/home/rest/searchBooks";

    public List<Book> searchByBookName(String bookName) throws ErrorResponseInfo {
        HttpGet request = new HttpGet(URL_PATH+"/searchByBookName?book_name=" + bookName);
        return result(request);
    }

    public List<Book> searchByAuthorName(String authorName) throws ErrorResponseInfo {
        HttpGet request = new HttpGet(URL_PATH+"/searchByAuthorName?author_name=" + authorName);
        return result(request);
    }

    public List<Book> searchByIsbn(String isbn) throws ErrorResponseInfo {
        HttpGet request = new HttpGet(URL_PATH+"/searchByIsbn?isbn=" + isbn);
        return result(request);
    }

    public List<Book> searchByYearRange(int startYear, int endYear) throws ErrorResponseInfo {
        HttpGet request = new HttpGet(URL_PATH+"/searchByYearRange?start_year=" + startYear + "&end_year=" + endYear);
        return result(request);
    }

    public List<Book> searchInSeveralWays(int year, int pageCount, String bookName) throws ErrorResponseInfo {
        HttpGet request = new HttpGet(URL_PATH+"/searchInSeveralWays?year=" + year + "&page_count=" + pageCount + "&book_name=" + bookName);
        return result(request);
    }

    public List<Book> searchByUserBookmarks(int user_id) throws ErrorResponseInfo {
        HttpGet request = new HttpGet(URL_PATH+"/searchByUserBookmarks?user_id=" + user_id);
        return result(request);
    }


    private List<Book> result(HttpGet request) throws ErrorResponseInfo {
        String result = "server error";
        GsonBuilder builder = new GsonBuilder().setDateFormat("mm.dd.yyyy");
        Gson gson = builder.create();
        try (CloseableHttpClient httpClient = HttpClients.createDefault();
             CloseableHttpResponse response = httpClient.execute(request)) {

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                result = EntityUtils.toString(entity);

                Book[] books = gson.fromJson(result, Book[].class);
                return Arrays.asList(books);
            }
        } catch (IOException e) {
            System.out.println(result);
        } catch (JsonSyntaxException e) {
            throw gson.fromJson(result, ErrorResponseInfo.class);
        }
        return new ArrayList<>();
    }


}




