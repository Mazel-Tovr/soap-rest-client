package com.epam.rest.model;

public class Book {

    private int id;
    private String bookName;
    private int releaseYear;
    private String ISBN;
    private String publisher;
    private int pageCount;
    private Author author;

    public Book(int id, String bookName, int releaseYear, String ISBN, String publisher, int pageCount, Author author) {
        this.id = id;
        this.bookName = bookName;
        this.releaseYear = releaseYear;
        this.ISBN = ISBN;
        this.publisher = publisher;
        this.pageCount = pageCount;
        this.author = author;
    }

    public Book() {
    }

    public int getId() {
        return id;
    }

    public String getBookName() {
        return bookName;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public String getISBN() {
        return ISBN;
    }

    public String getPublisher() {
        return publisher;
    }

    public int getPageCount() {
        return pageCount;
    }

    public Author getAuthor() {
        return author;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", bookName='" + bookName + '\'' +
                ", releaseYear=" + releaseYear +
                ", ISBN='" + ISBN + '\'' +
                ", publisher='" + publisher + '\'' +
                ", pageCount=" + pageCount +
                ", author=" + author +
                '}';
    }

}
