package com.epam.rest.model;

public class Token
{
  private String token;
  private String httpStatus;
  private String message;

    public Token(String token, String httpStatus, String message) {
        this.token = token;
        this.httpStatus = httpStatus;
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public String getHttpStatus() {
        return httpStatus;
    }

    public String getMessage() {
        return message;
    }
}
