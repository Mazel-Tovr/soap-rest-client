package com.epam.rest.model;

public class History {
    private int id;
    private int userId;
    private String actionText;

    public int getId() {
        return id;
    }

    public int getUserId() {
        return userId;
    }

    public String getActionText() {
        return actionText;
    }

    @Override
    public String toString() {
        return "History{" +
                "id=" + id +
                ", userId=" + userId +
                ", actionText='" + actionText + '\'' +
                '}';
    }
}
