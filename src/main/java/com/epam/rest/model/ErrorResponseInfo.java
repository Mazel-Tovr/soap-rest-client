package com.epam.rest.model;

public class ErrorResponseInfo extends Exception{
    private int errorCode;
    private int httpStatus;
    private String message;

    public ErrorResponseInfo(int errorCode, int httpStatus, String message) {
        this.errorCode = errorCode;
        this.httpStatus = httpStatus;
        this.message = message;
    }

    @Override
    public String toString() {
        return "ErrorResponseInfo{" +
                "errorCode=" + errorCode +
                ", httpStatus=" + httpStatus +
                ", message='" + message + '\'' +
                '}';
    }
}
