package com.epam.rest;

import com.epam.rest.clients.*;
import com.epam.rest.model.ErrorResponseInfo;
import com.epam.rest.model.Token;

import java.io.UnsupportedEncodingException;

/**
 * Hello world!
 */
public class TestRESTClients {
    public static void main(String[] args) {
      // search();
        //admin();
        // bookmark();
        //author();
        book();
    }


    private static void search()  {
        try {
            SearchClient search = new SearchClient();
             search.searchByBookName("w").forEach(System.out::println);
            // search.searchByUserBookmarks(8).forEach(System.out::println);
             //search.searchByAuthorName("a").forEach(System.out::println);
            // search.searchByIsbn("000-0000000008").forEach(System.out::println);
            //search.searchByYearRange(1, 2020).forEach(System.out::println);
            // search.searchInSeveralWays(2002, 1275, "Harry").forEach(System.out::println);
        }catch (ErrorResponseInfo e)
        {
            System.out.println(e.toString());
        }

    }

    private static Token login() {
        try {
            //"roma","123
            return new UserClient().login("1", "1");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static void admin() {
        AdminClient adminClient = new AdminClient();
        Token token = login();
         // System.out.println(adminClient.registerNewUser("roma","123",token));
        //adminClient.getAll(4, token).forEach(System.out::println);
        //System.out.println(adminClient.blockUser(4,token));
    }

    private static void author() {
        Token token = login();
        AuthorClient authorClient = new AuthorClient();
       // System.out.println(authorClient.addAuthor("URa", "lixachov", "", "1999-07-07", token));
       // System.out.println(authorClient.deleteAuthor(9, token));
       //authorClient.getAll().forEach(System.out::println);

    }

    private static void bookmark() {
        Token token = login();
        BookmarkClient bookmarkClient = new BookmarkClient();
       // System.out.println(bookmarkClient.addBookmark(4, 8, 440, token));
        //System.out.println(bookmarkClient.deleteBookmark(0,token));
        //bookmarkClient.getAll().forEach(System.out::println);
    }

    private static void book() {
        Token token = login();
        BookClient bookClient = new BookClient();
        //System.out.println(bookClient.addBook("name",2010,"000-1111111111","Ya",20,1,token));

       // System.out.println(bookClient.deleteBook(0,token));

        bookClient.getAll().forEach(System.out::println);
    }

}
